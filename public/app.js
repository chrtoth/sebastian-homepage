var scene = new THREE.Scene();
THREE.Cache.enabled = true
THREE.Cache.add("bg", "/ul/ul_1.jpg_bg")

var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
camera.position.z = 4;

var renderer = new THREE.WebGLRenderer({alpha:true,antialias:true});

renderer.setClearColor("#111111",0);

renderer.setSize( window.innerWidth, window.innerHeight );

document.body.appendChild( renderer.domElement );

// CUBE
var geometry = new THREE.BoxGeometry(1.5,1.5,1.5 );
var material = new THREE.MeshBasicMaterial( { wireframe: true,color: "#FFFFFF" , wireframeLinewidth:10} );
var cube = new THREE.Mesh( geometry, material );

scene.add( cube );

//plane
var geometry = new THREE.PlaneGeometry( 300, 300 );
texture = THREE.ImageUtils.loadTexture(THREE.Cache.get("bg"));
material = new THREE.MeshBasicMaterial({map: texture});
var plane = new THREE.Mesh(geometry, material);
plane.position.z = -75
//scene.add( plane );


var mouse = new THREE.Vector2();
function onMouseMove( event ) 
{

	mouse.x = ( event.clientX / window.innerWidth ) * 2 - 1;
	mouse.y = - ( event.clientY / window.innerHeight ) * 2 + 1;

  plane.position.x = mouse.x*-10
  plane.position.y = mouse.y*-10

}

function mouse_click(e)
{

}
window.addEventListener( 'mousemove', onMouseMove, false );
window.addEventListener( 'onlick', mouse_click, false)

var render = function () {
  requestAnimationFrame( render );

  width = window.innerWidth;
  count = 6
  pos = width/mouse.x
  m_x = (width*(mouse.x+1)/2)
  id = Math.floor((m_x/width*6)+1)
  for( var i = 1; i<=count; i++)
  {
    elem = document.getElementById(i)
    if ( i == id)
    {
      elem.classList.add("selected")
    }
    else
    {
      elem.classList.remove("selected")

    }

  }




  cube.rotation.x = -mouse.y*5;
  cube.rotation.y = mouse.x*5;

  renderer.render(scene, camera);
};

if ( !("disable_effects" in window) || disable_effects  != true )
{
	render();
}
